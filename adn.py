#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
permet de vérifier qu'une chaîne de caractère correspond à de l'ADN
"""

__author__ = 'Adeline Kalic'



def is_valid(adn_str):
    if "t" and "a" and "g" and "c" in adn_str:
        return True
    else:
        return False
	
#is_valid("agctt")

def get_valid_adn(prompt='chaîne : '):
    #print(prompt)
    ADN=input(prompt)
    var=is_valid(ADN)
    #print(var)
    while var != True:
        ADN=input(prompt)
        var=is_valid(ADN)
        
get_valid_adn()